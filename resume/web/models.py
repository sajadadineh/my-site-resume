# from __future__ import unicode_literals
from django.db import models
# from django.contrib.auth.models import User

# Create your models here.

class infomationSocialMedia(models.Model):
    name = models.CharField(max_length=255)
    link = models.TextField()
    icone = models.CharField(max_length=255)
    def __str__ (self):
        return self.name

class profileSideBar(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=255)
    bio = models.TextField()
    def __str__(self):
        return self.name

class banner (models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class about (models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class hobbies(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class coffee(models.Model):
    cupsOfCoffee = models.BigIntegerField()
    project = models.BigIntegerField()
    client = models.BigIntegerField()
    def __str__(self):
        return self.cupsOfCoffee

class skillDescription (models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class skills (models.Model):
    titelSkills = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    def __str__(self):
        return self.titelSkills

class education(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    time = models.CharField(max_length=255)
    def __str__(self):
        return self.title

class experienceDescription(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class experience(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    time = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    def __str__(self):
        return self.title

class siteRepo(models.Model):
    title = models.CharField(max_length=255)
    link = models.TextField()
    def __str__(self):
        return self.title