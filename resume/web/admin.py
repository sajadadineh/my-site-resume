from django.contrib import admin
from .models import infomationSocialMedia, profileSideBar, banner, about, hobbies, coffee, skillDescription, skills, education, experienceDescription, experience, experienceDescription, siteRepo

# Register your models here.

admin.site.register(infomationSocialMedia)
admin.site.register(profileSideBar)
admin.site.register(banner)
admin.site.register(about)
admin.site.register(hobbies)
admin.site.register(coffee)
admin.site.register(skillDescription)
admin.site.register(skills)
admin.site.register(education)
admin.site.register(experienceDescription)
admin.site.register(experience)
admin.site.register(siteRepo)

