# my-site-resume

[Resume site link](https://sajadadineh.ir/)

### Description

This site is a personal resume written with HTML, CSS and JavaScript,
This is a ready-made [template](https://colorlib.com/) that I have changed and deployed in [fandoghe](https://www.fandogh.cloud/)

### Todo list

- [ ] use Django for panel admin
- [ ] make Portfolio view pic
- [ ] make and connect cantact to the my protonmail
- [ ] Post a photo for title website

## How to use it?

The documentation of the [fandoghe](https://docs.fandogh.cloud/docs/getting-started.html) site fully explains how to make this site and deploy with the help of [Docker](https://www.docker.com/)
